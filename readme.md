# Pragmatic Python
## By Phillip Alday

# Links
* [Authomate the Boring Stuff with Python](http://automatetheboringstuff.com/)
* [The Git Parable](http://tom.preston-werner.com/2009/05/19/the-git-parable.html)
* [Snake Wrangling for Kids](http://briggs.net.nz/snake-wrangling-for-kids.html)
* [Hginit](http://hginit.com/)
* [Python Tutorial](http://docs.python.org/2/tutorial/index.html)
* [Python Library Reference](http://docs.python.org/2/library/index.html)
* [Google's Python Class](https://developers.google.com/edu/python/) (Nicht ganz aktuell)
* [Python Regex HOWTO](http://docs.python.org/2/howto/regex.html)
* [Stack Overflow](http://stackoverflow.com/) 
* [nltk](http://nltk.org/) und [Buch](http://nltk.org/book/)
* [Beautiful Soup](http://www.crummy.com/software/BeautifulSoup/)
* [OpenSesame](http://osdoc.cogsci.nl/)
* [suds](https://bitbucket.org/palday/suds) und [libleipzig](https://github.com/palday/libleipzig-python)
* [WinPython](https://code.google.com/p/winpython/) Python zum Mitnehmen auf dem USB-Stick -- inkl. TortoiseHG und Spyder 
* [Portable Python](http://portablepython.com/) minimalere Alternative zu WinPython
* [Spyder](https://code.google.com/p/spyderlib/)
* [Komodo Edit](http://www.activestate.com/komodo-edit)

# License

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 icense. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0 

The programs presented here are mostly so simple as to be trivial, and as such, they should not be able to cause any damage. However, as Murphy once noted, there is always the potential for something to go horribly wrong, and so I hereby explicitly state that 

**the code here (including but not limited to the code in the pages for the individual sessions) is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.**

Think before you type: I'm not responsible if you blindly copy and paste code and something bad happens.